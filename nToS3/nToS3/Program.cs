﻿using Ionic.Zip;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nToS3
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static async Task<int> Main(string[] args)
        {
            var exitcode = 0;
            var showGui = File.Exists(Config.FILENAME) == false;

            if (showGui)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
            else
            {
                try
                {
                    //get config
                    var config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(Config.FILENAME));

                    await Backup(config);
                }
                catch (Exception ex)
                {
                    File.WriteAllText("lastError.txt", string.Format("Last Error @{0}: {1}", DateTime.Now, ex.GetBaseException()));
                    exitcode = 1;
                }
            }

            return exitcode;
        }

        public static async Task Backup(Config config)
        {
            //zip file name
            string zipfilePath;
            if (config.Paths.Count() > 1)
                zipfilePath = string.Format("backup_{0}.zip", DateTime.Now.ToString("yyyyMMddTHHmmss"));
            else
                zipfilePath = string.Format("{0}_backup_{1}.zip", Path.GetFileName(config.FileToBackup), DateTime.Now.Day.ToString("D2"));


            //create zip file                                          
            using (var zip = new ZipFile())
            {
                foreach (var path in config.Paths)
                {
                    if (config.IsDir(path))
                        zip.AddDirectory(path);
                    else
                        zip.AddFile(path);
                }

                zip.Save(zipfilePath);
            }

            //upload to aws
            var aws = new AwsWrapper();
            await aws.Upload(zipfilePath, config.AwsAccessKey, config.AwsSecretKey, config.AwsS3BucketName);

            //delete local zip file
            File.Delete(zipfilePath);
        }
    }
}
