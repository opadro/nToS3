﻿using Amazon.S3;
using Amazon.S3.Model;
using System.IO;
using System.Threading.Tasks;

namespace nToS3
{
    /// <summary>
    /// Wraps AWS.
    /// </summary>
    public class AwsWrapper
    {
        public AwsWrapper()
        {

        }

        public async Task Upload(string filename, string accessKey, string secretKey, string bucketName)
        {
            using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.USEast1))
            {
                var fs = new FileStream(filename, FileMode.Open);

                var request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = S3CannedACL.Private,
                    Key = Path.GetFileName(filename),
                    InputStream = fs,
                };
                await client.PutObjectAsync(request);
            }
        }
    }
}
