﻿using System;
using System.Windows.Forms;

namespace nToS3
{
    public partial class MainForm : Form
    {
        public MainForm() => InitializeComponent();

        void MainForm_Load(object sender, EventArgs e)
        {

        }

        void selectFileButton_Click(object sender, EventArgs e)
        {
            var result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                var path = openFileDialog1.FileName;
                fileToBackupTextBox.Text = path;
            }
        }

        async void backupButton_Click(object sender, EventArgs e)
        {
            try
            {
                Enabled = false;

                //get config
                var config = new Config(fileToBackupTextBox.Text, namingConventionTextBox.Text,
                                         accessTexbox.Text, secretTextbox.Text, namingConventionTextBox.Text);

                await Program.Backup(config);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            finally
            {
                Enabled = true;
            }
        }
    }
}
