﻿namespace nToS3
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fileToBackupTextBox = new System.Windows.Forms.TextBox();
            this.namingConventionTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.secretTextbox = new System.Windows.Forms.TextBox();
            this.secretLabel = new System.Windows.Forms.Label();
            this.accessTexbox = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.bucketNameTextBox = new System.Windows.Forms.TextBox();
            this.selectFileButton = new System.Windows.Forms.Button();
            this.backupButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(15, 261);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(802, 230);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "File to backup:";
            // 
            // fileToBackupTextBox
            // 
            this.fileToBackupTextBox.Location = new System.Drawing.Point(117, 20);
            this.fileToBackupTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.fileToBackupTextBox.Name = "fileToBackupTextBox";
            this.fileToBackupTextBox.Size = new System.Drawing.Size(258, 20);
            this.fileToBackupTextBox.TabIndex = 3;
            // 
            // namingConventionTextBox
            // 
            this.namingConventionTextBox.Location = new System.Drawing.Point(117, 51);
            this.namingConventionTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.namingConventionTextBox.Name = "namingConventionTextBox";
            this.namingConventionTextBox.Size = new System.Drawing.Size(258, 20);
            this.namingConventionTextBox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Naming convention:";
            // 
            // secretTextbox
            // 
            this.secretTextbox.Location = new System.Drawing.Point(86, 52);
            this.secretTextbox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.secretTextbox.Name = "secretTextbox";
            this.secretTextbox.Size = new System.Drawing.Size(362, 20);
            this.secretTextbox.TabIndex = 9;
            // 
            // secretLabel
            // 
            this.secretLabel.AutoSize = true;
            this.secretLabel.Location = new System.Drawing.Point(8, 53);
            this.secretLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.secretLabel.Name = "secretLabel";
            this.secretLabel.Size = new System.Drawing.Size(62, 13);
            this.secretLabel.TabIndex = 8;
            this.secretLabel.Text = "Secret Key:";
            // 
            // accessTexbox
            // 
            this.accessTexbox.Location = new System.Drawing.Point(86, 23);
            this.accessTexbox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.accessTexbox.Name = "accessTexbox";
            this.accessTexbox.Size = new System.Drawing.Size(362, 20);
            this.accessTexbox.TabIndex = 7;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(8, 23);
            this.label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(66, 13);
            this.label.TabIndex = 6;
            this.label.Text = "Access Key:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nameLabel);
            this.groupBox1.Controls.Add(this.bucketNameTextBox);
            this.groupBox1.Controls.Add(this.label);
            this.groupBox1.Controls.Add(this.secretTextbox);
            this.groupBox1.Controls.Add(this.accessTexbox);
            this.groupBox1.Controls.Add(this.secretLabel);
            this.groupBox1.Location = new System.Drawing.Point(9, 79);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(478, 113);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "AWS";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(8, 80);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(73, 13);
            this.nameLabel.TabIndex = 11;
            this.nameLabel.Text = "Bucket name:";
            // 
            // bucketNameTextBox
            // 
            this.bucketNameTextBox.Location = new System.Drawing.Point(86, 78);
            this.bucketNameTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bucketNameTextBox.Name = "bucketNameTextBox";
            this.bucketNameTextBox.Size = new System.Drawing.Size(362, 20);
            this.bucketNameTextBox.TabIndex = 12;
            // 
            // selectFileButton
            // 
            this.selectFileButton.Location = new System.Drawing.Point(382, 18);
            this.selectFileButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.selectFileButton.Name = "selectFileButton";
            this.selectFileButton.Size = new System.Drawing.Size(116, 26);
            this.selectFileButton.TabIndex = 11;
            this.selectFileButton.Text = "Select file to backup";
            this.selectFileButton.UseVisualStyleBackColor = true;
            this.selectFileButton.Click += new System.EventHandler(this.selectFileButton_Click);
            // 
            // backupButton
            // 
            this.backupButton.Location = new System.Drawing.Point(369, 197);
            this.backupButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.backupButton.Name = "backupButton";
            this.backupButton.Size = new System.Drawing.Size(118, 46);
            this.backupButton.TabIndex = 12;
            this.backupButton.Text = "Backup to S3";
            this.backupButton.UseVisualStyleBackColor = true;
            this.backupButton.Click += new System.EventHandler(this.backupButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 520);
            this.Controls.Add(this.backupButton);
            this.Controls.Add(this.selectFileButton);
            this.Controls.Add(this.namingConventionTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fileToBackupTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "nToS3";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fileToBackupTextBox;
        private System.Windows.Forms.TextBox namingConventionTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox secretTextbox;
        private System.Windows.Forms.Label secretLabel;
        private System.Windows.Forms.TextBox accessTexbox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox bucketNameTextBox;
        private System.Windows.Forms.Button selectFileButton;
        private System.Windows.Forms.Button backupButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

