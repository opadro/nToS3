﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using nToS3;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class ConfigTests
    {
        [TestMethod]
        public void TestPaths()
        {
            var cfg = new Config();
            var dir = "c:\\MockFolder";
            var file = "d:\\MockDrive\\MockFile.bak";
            cfg.FileToBackup = string.Format("{0};{1}", dir, file);

            var paths = cfg.Paths.ToList();
            Assert.IsTrue(paths.Count() == 2);
            Assert.AreEqual(dir, paths[0]);
            Assert.AreEqual(file, paths[1]);
        }

        [TestMethod]
        public void TestOverload()
        {
            var dir = "c:\\MockFolder";
            var file = "d:\\MockDrive\\MockFile.bak";
            var cfg = new Config(dir, "monthday", "key", "secret", "myBucket")
            {
                FileToBackup = string.Format("{0};{1}", dir, file)
            };

            var paths = cfg.Paths.ToList();
            Assert.IsTrue(paths.Count() == 2);
            Assert.AreEqual(dir, paths[0]);
            Assert.AreEqual(file, paths[1]);

            //Check object has been constructed correctly
            Assert.IsInstanceOfType(cfg, typeof(Config));
            Assert.IsNotNull(cfg.AwsAccessKey);
            Assert.IsNotNull(cfg.AwsSecretKey);
            Assert.IsNotNull(cfg.AwsS3BucketName);
            Assert.IsNotNull(cfg.NamingConvention);
            Assert.IsNotNull(cfg.FileToBackup);
        }
    }
}
