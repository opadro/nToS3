﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTests
{
    [TestClass]
    public class DateTimeTests
    {
        [TestMethod]
        public void TestToString()
        {
            //ISO 8601
            //http://stackoverflow.com/questions/12500091/datetime-tostring-format-that-can-be-used-in-a-filename-or-extension

            var d = DateTime.Now;
            var expected = d.ToString("yyyyMMddTHHmmss");
            var actual = string.Format("{0}{1}{2}T{3}{4}{5}",
                d.Year, d.Month.ToString("00"), d.Day.ToString("00"), d.Hour.ToString("00"), d.Minute.ToString("00"), d.Second.ToString("00"));

            Assert.AreEqual(actual, expected);
        }
    }
}
