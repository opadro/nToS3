# nToS3
A .Net C# tool that can backup your files to AWS S3. Simple.

[![Build status](https://ci.appveyor.com/api/projects/status/lmmyp47i2ywv6e2b?svg=true)](https://ci.appveyor.com/project/opadro/ntos3)


## Long Description
If you need to backup some files to Amazon Web Services (AWS) Simple Storage Service (S3) using .Net, in a simple manner, this tool is for you. The file going to S3 is stored in a zip file and flagged as private. You can integrate this with your custom .Net solution or use it as a standalone app. As a standalone app, you can use the Windows Task Scheduler to automate when you want nToS3 to run and upload your files to S3.


### IT Pro: Use the GUI or the cmd prompt!
Say for example that you have an on premise server running SQL Server. You want to schedule the backup of this database into S3. You can use this tool and a batch file to tackle this problem.


#### Sample Batch File
Example of how nToS3 would fit in your workflow:

```sh
ECHO OFF
CLS
ECHO Running Nightly Job
ECHO STEP 1/4 - Stop IIS...
iisreset /stop

ECHO.
ECHO STEP 2/4 - Running backup and maintenance script on database...
sqlcmd -S SERVER_NAME -i "C:\Temp\sql_backup_and_maintenance_script.sql" -E

ECHO.
ECHO STEP 3/4 - Start IIS...
iisreset /start

ECHO.
ECHO STEP 4/4 - Creating DB zip file and uploading to S3 with nToS3!...
pushd c:\temp
nToS3.exe

ECHO.
ECHO Job Successful.
ECHO ON
```


### Developer: Just reference the .exe!
In your Visual Studio project you can add a reference to `nToS3.exe`, as you do with other DLLs, and access its features.


## Configuration 
nToS3 likes JSON. Place a JSON file named `config.json` in the same directory with nToS3. Your file should look like this:

```sh
{
"FileToBackup":"x:\\path_to_files_and_or_directories__dont_forget_to_escape_the_backslash.docx"
"NamingConvention":"dayofmonth",
"AwsAccessKey": "your aws key",
"AwsSecretKey": "your aws secret",
"AwsS3BucketName": "your aws bucket"
}
```


## Error Logging
nToS3 will store the last exception (error) in the `lastError.txt` file. This file can be found in the same directory where `nToS3.exe` is.


## Build
To build the source code you need at least Visual Studio Studio 2019 Community Edition. NuGet will download all other dependencies.


## Installation | Deploy
* Download the `ntos3.zip` file from here: https://github.com/opadro/nToS3/releases/latest. Make sure your target machine has the .Net Framework 4.7.2 installed.

## FAQ
### Can I backup one file, multiple files, or a directory?
Yes. You can type the path to various files or directories separated by `;`
Example: `"FileToBackup":"c:\\SomeFolder;d:\\SomeFile.bak";`

### Side-by-Side Installations
Sure. Copy the app, along with its dependencies, to other folders. Then configure each instance independently through the `config.json` file.

## License
MIT
